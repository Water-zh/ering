var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http');
const csrf = require('csurf');

app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//session set
app.set('trust proxy', 1);
app.set('appList', [
	// {
	// 	name : '管理',
	// 	uid : 'account manager'.toString('base64'),
	// 	link : '/system/account'
	// },
	{
		name : '帳號管理',
		uid : 'account manager'.toString('base64'),
		link : '/system/account'
	},
  {
		name : '權限管理',
		uid : 'account role'.toString('base64'),
		link : '/system/role'
  }
]);

//mqtt 
require('./lib/mqtt/'); 

app.use(session({
    secret: 'dsijewn2lk3-0djf332m4@ff',
    resave: false,
    saveUninitialized: true,
}));
//DEBUG=eRing:* npm start
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

csrfProtection = csrf();
parseForm = bodyParser.urlencoded({ extended: false });
app.use(parseForm);
//require routers 
require('./routes');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const server = http.createServer(app);

server.listen(8080);
