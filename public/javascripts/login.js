(function(){
    var elements = [
         document.querySelector('button#backBtn'),
         document.querySelector('a#forget-password')
    ];
    var clickHandle = function(){
        var _this = this;
        var showClass = '', hideClass = '';
        
        switch( _this.id ){
            case 'forget-password':
                showClass = 'forget-form', hideClass = 'login-form';
                break;
            case 'backBtn':
                showClass = 'login-form', hideClass = 'forget-form';
                break;
        }
        
        document.querySelector(`form.${showClass}`).style.display = 'block';
        document.querySelector(`form.${hideClass}`).style.display = 'none';  
    };

    for(key in elements){
        elements[key].addEventListener('click', clickHandle);
    }

})();