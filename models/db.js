const Sequelize = require('sequelize');
const mysql = require('mysql');
/**
*  * @param {TableName}   DataBase  connect db 
*/
var db = function( table , colType){
    this.tableName = table;
    let _seq = new Sequelize('homestead','root','secret',{
            host : 'localhost',
            port: 3306,
            dialect : 'mariadb',
            pool:{
                max: 5,
                min: 0,
                idle:10000
            }
        });

    let thisModel = _seq.define(table, colType);

    //將要儲存的欄位和資料用json格式傳入
    /*
        {
            title : 'test',
            is_visible : 1
        }
    */
    /** 
     * 
     *  * @param {create}   model  create data 
    */
    this.create = ( data )=>{
        return thisModel.build( data ).save();
    };

    //model 是 Sequelize find 後的物件
    //data 是 要update 的資料 { title : 'test', is_visible : 1 }
    this.update = (model, data)=>{
        return model.update(data);
    };

    //抓取所有資料，如果沒有下篩選就抓全部欄位
    this.findAllData = (fileds)=>{
        let data = thisModel.findAll({
            attributes: fileds
        });
        return data;
    };
    //欄位去找出單筆資料
    this.findOneByWhere = (col , attr)=>{
        return thisModel.findOne({ where : col, attributes : attr });
    };
    //抓單筆資料，用「then」獲取 Promise 資料
    this.findOne = (id)=>{
        return thisModel.findById(id);
    },
    //刪除單筆資料
    this.deleteOne = (uuid)=>{
        return thisModel.destroy({
            where : { id : uuid },
            truncate : true
        });
    }
};
module.exports = db;
