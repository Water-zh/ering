// login page
app.get('/', csrfProtection, (req, res, next)=>{
	console.log( `req.ip : ${req.ip}` );
	
	//如果已經登入session 還存在就轉到功能頁
	if ( req.session.user ) res.redirect('/system');

	const captcha = require('svg-captcha');
	var svgCaptcha = captcha.create();
	let message = req.session.errMessage || '';

	req.session.errMessage = '';
	req.session.captcha = svgCaptcha.text;

  	res.render('login', { 
    	_csrf : req.csrfToken(),
		captcha : svgCaptcha.data,
		errMessage : message
  	});
});

//檢查是否有登入，與檢查權限
app.use('/system*', (req, res, next)=>{
	if( !req.session.user ){
		res.redirect('/');
	}else{
		res.locals.menuList = app.get('appList');
		next();
	}
});
app.get('/system', (req, res, next)=>{
	res.render('index');
});
app.get('/system/account', require('../lib/account'));