# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.19-MariaDB)
# Database: homestead
# Generation Time: 2017-01-16 09:29:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table permissiions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissiions`;

CREATE TABLE `permissiions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '[fk]所屬會員帳號',
  `app_id` varchar(200) DEFAULT NULL COMMENT '功能權限',
  `createdAt` datetime DEFAULT NULL COMMENT '建立日期',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `title`, `link`, `createdAt`, `updatedAt`)
VALUES
	(1,'分享我用cnode社区api做微信小应用的入门过程','/topic/57ea257b3670ca3f44c5beb6','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(2,'《一起学 Node.js》彻底重写完毕','/topic/581b0c4ebb9452c9052e7acb','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(3,'Node.js已经启动了第二个用户调查，它将保持打开，直到12月底。','/topic/584f21733ebad99b336b1fdb','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(4,'这，就是技术人的江湖','/topic/580ddc2eeae2a24f34e67e69','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(5,'少年，不要滥用箭头函数啊','/topic/584a207a3ebad99b336b1ede','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(6,'模仿cnode,基于koa2写了个小论坛项目!','/topic/584ccc3a9ff0dbf333450984','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(7,'angularjs交流社区qq:482133340','/topic/584f9c7a4c17b38d35436607','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(8,'socket.io,通知系统，多页面，请进！socketid的问题','/topic/584f92bc9ff0dbf333450a05','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(9,'注意rest参数是参数不是数组','/topic/584e52d3f3576dd333f86bf6','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(12,'话说大家用node开发后端都用过哪些orm','/topic/584682969ff0dbf333450813','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(13,'服务端使用socket.io编写，客户端也必须使用socket.io吗，可以直接使用浏览器提供的websocket对象吗？','/topic/51d29d5dd44cbfa304168f66','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(14,'关于html实体','/topic/584d7191f3576dd333f86bb3','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(15,'nodejs中文间隔号问题','/topic/584f4da8f3576dd333f86c18','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(16,'富文本编辑器在IE下会自动添加超链接，这个问题怎么解决？','/topic/584e1aa7f3576dd333f86bc6','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(17,'老司机教你如何用node.js抢HR的饭碗！','/topic/584e71bf9ff0dbf3334509df','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(18,'请问nodejs 是否有server端的模板，类似 ruby的erb','/topic/584f51cd3ebad99b336b1fe7','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(19,'express搭建https出现的问题','/topic/584a275d9ff0dbf333450909','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(20,'详细讲解nodejs中使用socket的私聊的方式','/topic/557a999216839d2d539361a3','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(21,'用socket.io做通知系统，请大家看下我的做法有没有问题？','/topic/584e28e23ebad99b336b1f9d','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(22,'新手求助--在nodejs里想用git版本控制保存代码，除了传统的git方法一个文件一个文件的保存，还有更便捷的方法嘛？','/topic/584e6b3c3ebad99b336b1fb8','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(23,'自己写的一些工具库，已经用于公司的商业项目。在此分享，不用赞我','/topic/584d01b09ff0dbf33345098a','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(24,'Koa或者node里有控制访问页面权限的中间件吗？','/topic/584f49ad4c17b38d354365de','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(25,'分享一个与众不同的虚拟物品售卖产品 云售','/topic/584eb013f3576dd333f86c11','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(26,'编写一个简单的 Redis 客户端','/topic/573b5482b507f69e1dd89fcb','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(27,'彻底跨平台，Node.js高效生成验证码，我自己用纯js实现了一个图形模块','/topic/581b2502e90cfbec054d763f','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(28,'求助：node.js vm 模块在什么情况下使用？','/topic/540444820256839f7112fc9a','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(29,'socket客户端接收不到服务器端传来的数据？','/topic/584e60734c17b38d354365c3','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(30,'websocket 连接 socket.io问题','/topic/538d9bbcc3ee0b58204d6dea','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(31,'使用pm2 实现自动化的多服务器部署--express application','/topic/584e5ee74c17b38d354365c2','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(32,'大家有使用Mapbox-gl.js，设置字体为中文，导致加载很慢','/topic/584e493df3576dd333f86be1','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(33,'Ajax更新了路由，但是res.render没有重绘页面','/topic/584c159e3ebad99b336b1f5d','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(34,'nodejs中数据库连接池的问题；','/topic/584e11603ebad99b336b1f93','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(35,'这里发布一个话题会不会在https://cnodejs.org/api/v1 显示','/topic/584e53a84c17b38d354365bf','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(36,'为什么写在脚本里的npm命令会找不到呢?','/topic/584dfbd73ebad99b336b1f8a','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(37,'Koa 请求日志打点工具','/topic/58108b69b37ee8fb33978839','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(38,'［ 北京］12月11日 node-party @ 北新桥科技寺，报名从速 !','/topic/583b8e4227d001d606ac19f0','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(39,'关于net._createServerHandle的困惑','/topic/584be6ebf3576dd333f86b91','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(40,'NodeJS是否适合做API网关','/topic/5790a89e43fb9f04148cfd1d','2016-12-13 07:58:57','2016-12-13 07:58:57'),
	(41,'Test1111111111---','/topic/584fadeb3ebad99b336b201d','2016-12-13 08:48:29','2016-12-13 08:48:29'),
	(42,'单点登录的三种实现方式','/topic/55f6e69904556da7553d20dd','2016-12-13 08:48:29','2016-12-13 08:48:29'),
	(43,'求推荐比较好的压测工具','/topic/58213781be0a73ad05489590','2016-12-13 08:48:29','2016-12-13 08:48:29'),
	(44,'nodejs是啥神器','/topic/584fad364c17b38d3543660b','2016-12-13 08:48:29','2016-12-13 08:48:29'),
	(45,'node程序的模块能否以依赖注入的形式获取？不必 require 一堆文件','/topic/584fb32f3ebad99b336b2021','2016-12-13 08:48:29','2016-12-13 08:48:29'),
	(46,'应该在 babel 前 test 还是之后呢？','/topic/584faa943ebad99b336b2015','2016-12-13 08:48:29','2016-12-13 08:48:29');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '序號',
  `is_visible` int(11) DEFAULT '0' COMMENT '是否啟用',
  `account` tinytext COMMENT '帳號',
  `password` varchar(300) DEFAULT NULL COMMENT '密碼',
  `mail` tinytext COMMENT '信箱',
  `createdAt` datetime DEFAULT NULL COMMENT '建立時間',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
