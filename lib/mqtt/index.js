var mosca = require('mosca')
var settings = {
        port: 5112
    },
    ips = [];
var server = new mosca.Server(settings);
server.on('ready', function(){
	console.log('Mosca server running..');	
});

server.on('published', function(packet, client) {
    if( client ){
        switch( packet.topic ){
            case 'sendIP': 
                var clientIP = packet.payload.toString();
                
                console.log( clientIP );
                if( clientIP && !ips.indexOf(clientIP) > -1  ) ips.push( clientIP );
                console.log(ips);
                server.publish({
                    topic : `ring/${clientIP}`,
                    payload : `Hello, ${client.id}`,
                    retain : packet.retain,
                    qos: packet.qos
                }, client);
                break;
        }

    }

});