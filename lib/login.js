const Sequelize = require('sequelize');
const bcrypt = require('bcrypt-nodejs');
const db = require('../models/db');

var user = new db('users',{
    is_visible : {type: Sequelize.INTEGER},
    account: {type: Sequelize.STRING},
    password: {type: Sequelize.STRING},
    mail: {type: Sequelize.STRING}
});

module.exports = (req, res, next)=>{
    if( req.body.captcha !== req.session.captcha || req.body.captcha == '' ){
        req.session.errMessage =  `驗證碼輸入錯誤或空白.`;
        res.redirect('/');
    }else{
        //查帳號
        user.findOneByWhere({ account : req.body.username }, ['is_visible','account','password']).then((Data)=>{
            //退回首頁登入畫面的狀況
            //1.找不到帳號資料
            //2.帳號狀態尚未變成啟用
            //3.表單密碼輸入錯誤
            if( !Data ) req.session.errMessage =  `找不到此帳號請與管理者聯絡.`,res.redirect('/');
            if( Data.is_visible === 0 ) req.session.errMessage =  `此帳號尚未啟用，請與管理者聯絡.`,res.redirect('/');
            if( !bcrypt.compareSync(req.body.Password, Data.password) ){ 
                req.session.errMessage =  `密碼錯誤.`,res.redirect('/');
            }else{
                req.session.user = Data;
                res.redirect('/system');
            }


        });
    }

};